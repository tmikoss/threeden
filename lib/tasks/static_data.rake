namespace :static_data do
  desc "Update static data"
  task update: :environment do
    require 'mysql2'

    client = Mysql2::Client.new(host: "localhost", username: 'root', database: 'eve')

    solar_systems_query = <<-SQL
      SELECT ss.solarSystemID, ss.solarSystemName, ss.x, ss.y, ss.z, ss.radius, r.regionName, ss.security
        FROM mapSolarSystems ss, mapRegions r
       WHERE r.regionID < 11000000
         AND r.regionID = ss.regionID
       ORDER BY ROUND(ss.security, 1) DESC, ss.solarSystemName ASC
    SQL

    jump_query = <<-SQL
      SELECT fromSolarSystemID, toSolarSystemID from mapSolarSystemJumps
    SQL

    jump_map = Hash.new { |hash, key| hash[key] = [] }
    client.query(jump_query).each do |row|
      jump_map[row['fromSolarSystemID']] << row['toSolarSystemID']
    end

    scaled_columns = %w(x y z radius)
    scale_factor   = 1000000000000000

    results = []
    client.query(solar_systems_query).each do |row|
      solar_system = [row['solarSystemID'], row['solarSystemName']]
      scaled_columns.each do |column|
        solar_system << (row[column] / scale_factor)
      end
      solar_system << row['regionName']
      solar_system << row['security']
      solar_system << jump_map[row['solarSystemID']]

      results << solar_system
    end

    File.open(Rails.root.join('public', 'data', 'solar_systems.json'), 'w') { |file| file.write MultiJson.dump results }
  end
end
