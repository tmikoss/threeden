class Renderer
  fov: 90
  spinSpeed: 0.00005
  animationQueue: []
  cameraDistance: 500
  cameraDistanceGoal: 500
  cameraTarget: new THREE.Vector3
  cameraTargetGoal: new THREE.Vector3
  singletons: {}

  constructor: ->
    @scene    = new THREE.Scene()
    @camera   = new THREE.PerspectiveCamera @fov
    @renderer = new THREE.WebGLRenderer
      antialias: true

    setInterval @animateCameraTarget, 10

  add: (object) -> @scene.add object

  addSingleton: (key, object) ->
    @scene.remove @singletons[key]
    @singletons[key] = object
    # scale = 0
    # scaleOut = ->
    #   scale += 0.01
    #   object.scale.set scale, scale, scale
    #   if scale < 1
    #     setTimeout scaleOut, 10
    # scaleOut()
    @scene.add @singletons[key]

  setSize: (w, h) -> @renderer.setSize w, h

  cameraRotationMatrix: ->
    matrix = new THREE.Matrix4()
    matrix.makeRotationFromQuaternion @camera.rotation._quaternion
    matrix

  render: (time) =>
    cameraSpin = new THREE.Vector3 @cameraDistance * Math.cos(@spinSpeed * time),
                                   @cameraDistance * Math.sin(@spinSpeed * time),
                                   @cameraDistance * Math.sin(@spinSpeed * time)

    @camera.position = cameraSpin.add @cameraTarget
    @camera.lookAt @cameraTarget
    @renderer.render @scene, @camera

    requestAnimationFrame @render

  calibrateCamera: =>
    @camera.aspect = window.innerWidth / window.innerHeight
    @camera.updateProjectionMatrix()
    @renderer.setSize window.innerWidth, window.innerHeight

  animateCameraTo: (target, distance=@cameraDistance) ->
    stepCount = 50

    vector       = target.clone().sub @cameraTargetGoal
    distanceDiff = distance - @cameraDistance

    @cameraTargetGoal   = target
    @cameraDistanceGoal = distance

    step = vector.divideScalar stepCount
    stepDistance = distanceDiff / stepCount
    f = =>
      @cameraDistance += stepDistance
      @cameraTarget.add step
    @animationQueue.push f for [1..stepCount]

  animateCameraTarget: =>
    @animationQueue.shift()?()

window.TDN ||= {}
window.TDN.Renderer = new Renderer()

$ ->
  TDN.Renderer.calibrateCamera()
  $('body').append TDN.Renderer.renderer.domElement
  TDN.Renderer.render()

  $(window).on 'resize', TDN.Renderer.calibrateCamera
