#= require jquery
#= require jquery_ujs
#= require lodash
#= require select2
#= require vendor/backbone.min
#= require vendor/three.min
#= require vendor/helvetiker_regular.typeface
#= require_tree .

TDN.SolarSystems.on 'galaxyReady', ->
  TDN.selectBox = $ '#selector'

  TDN.selectBox.select2
    query: TDN.SolarSystems.autocomplete
    placeholder: 'Show me...'
    allowClear: true
    initSelection: (element, callback) -> callback(element)

  TDN.selectBox.on 'change', (e) ->
    if e.val
      Backbone.history.navigate "systems/#{e.val}", { trigger: true }
    else
      Backbone.history.navigate "systems", { trigger: true }

  $('#random').on 'click', ->
    Backbone.history.navigate "systems/#{TDN.SolarSystems.random().get 'name'}", { trigger: true }

  $('#controls').show()

  Backbone.history.start()
