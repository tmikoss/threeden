starMaterial = new THREE.ParticleBasicMaterial
  vertexColors: true
  size: 5
  map: THREE.ImageUtils.loadTexture "images/particle.png"
  blending: THREE.AdditiveBlending
  transparent: true

textMaterial = new THREE.MeshBasicMaterial
  color: 0xffffff
  overdraw: true
  transparent: true
  opacity: 0.7
  depthTest: false

lineMaterial = new THREE.LineBasicMaterial
  vertexColors: true
  linewidth: 5

nameOffsetVector   = new THREE.Vector3 2, -0.15, 0
regionOffsetVector = new THREE.Vector3 2, -0.5, 0
zeroVector         = new THREE.Vector3

class SolarSystem extends Backbone.Model
  parse: (item) ->
    id:     item[0]
    name:   item[1]
    center: new THREE.Vector3 item[2], item[3], item[4]
    size:   item[5]
    region: item[6]
    color:  TDN.Colors.at item[7]
    linked: item[8]

  initialize: ->
    @fullName = "#{@get 'name'} (#{@get 'region'})"

  focus: ->
    @buildText()
    @buildLinks()
    TDN.Renderer.animateCameraTo @get('center'), 5

  forSelect: ->
    { id: @get('name'), text: @get('name') }

  buildText: ->
    nameText   = new THREE.TextGeometry @get('name'),   { size: 0.5, height: 0.01, font: "helvetiker" }
    regionText = new THREE.TextGeometry @get('region'), { size: 0.2, height: 0.01, font: "helvetiker" }

    vertex.add(nameOffsetVector)   for vertex in nameText.vertices
    vertex.add(regionOffsetVector) for vertex in regionText.vertices

    rotationMatrix = TDN.Renderer.cameraRotationMatrix()

    nameText.applyMatrix rotationMatrix
    regionText.applyMatrix rotationMatrix

    THREE.GeometryUtils.merge nameText, regionText

    textMesh = new THREE.Mesh nameText, textMaterial
    textMesh.position.add @get('center')

    TDN.Renderer.addSingleton 'system_text', textMesh

  buildLinks: ->
    linkedIds     = @get 'linked'
    linkedSystems = TDN.SolarSystems.filter (solarSystem) -> solarSystem.id in linkedIds

    links = new THREE.Object3D()
    links.position = @get 'center'
    for model in linkedSystems
      geometry = new THREE.Geometry()

      endpoint = model.get('center').clone().sub @get('center')
      nearCenter = endpoint.clone().setLength 0.05

      geometry.vertices.push nearCenter
      geometry.vertices.push endpoint

      geometry.colors.push @get 'color'
      geometry.colors.push model.get 'color'

      links.add new THREE.Line geometry, lineMaterial

    TDN.Renderer.addSingleton 'system_links', links

class SolarSystems extends Backbone.Collection
  model: SolarSystem
  url: '/data/solar_systems.json'

  initialize: ->
    @fetch { success: @buildGalaxy }

  buildGalaxy: =>
    geometry = new THREE.Geometry()

    for model in @models
      geometry.vertices.push model.get 'center'
      geometry.colors.push model.get 'color'

    particleSystem = new THREE.ParticleSystem geometry, starMaterial
    particleSystem.sortParticles = true

    TDN.Renderer.add particleSystem

    jQuery => @trigger 'galaxyReady'

  random: => @models[_.random 0, @length - 1]

  focusOnGalaxy: ->
    TDN.Renderer.addSingleton 'system_text', null
    TDN.Renderer.animateCameraTo zeroVector, 500

  autocomplete: (query) =>
    regex = new RegExp query.term, "i"

    matches = @models.filter (model) -> model.fullName.match regex
    limited = _.first matches, 50

    results = []
    for region, systems of _.groupBy(limited, (system) -> system.get 'region')
      results.push { text: region, children: _.map(systems, (system) -> system.forSelect())}

    query.callback { results: results }

window.TDN ||= {}
window.TDN.SolarSystems = new SolarSystems()
