class Router extends Backbone.Router
  routes:
    'systems':         'focusOnGalaxy'
    'systems/:name': 'focusOnSystem'

  focusOnGalaxy: ->
    TDN.SolarSystems.focusOnGalaxy()

  focusOnSystem: (name) ->
    system = TDN.SolarSystems.findWhere(name: name)
    TDN.selectBox.select2 'data', system.forSelect()
    system.focus()

window.TDN ||= {}
window.TDN.Router = new Router()
