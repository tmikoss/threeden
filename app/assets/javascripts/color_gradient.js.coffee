#= require vendor/rainbowvis

class ColorGradient
  steps: 20
  from: 'red'
  to: 'green'

  constructor: ->
    @rainbow = new Rainbow()
    @rainbow.setNumberRange -1 * @steps, @steps
    @rainbow.setSpectrum @from, @to

  hexAt: (i) -> @rainbow.colourAt Math.round i * @steps

  at: (i) -> new THREE.Color "##{@hexAt i}"

window.TDN ||= {}
window.TDN.Colors = new ColorGradient()
